<?php

/**
 * WordPress settings API demo class
 *
 * @author PureDevs
 */
if ( !class_exists('Pure_GDPR_API_Settings' ) ):
class Pure_GDPR_API_Settings {

    private $settings_api;

    function __construct() {
        $this->settings_api = new Pure_GDPR_Settings_API_Class;

        add_action( 'admin_init', array($this, 'admin_init') );
        add_action( 'admin_menu', array($this, 'admin_menu') );
	    add_action( 'admin_bar_menu', array($this,'pd_admin_bar_menu'), 2000 );
    }

    function admin_init() {

        //set the settings
        $this->settings_api->set_sections( $this->get_settings_sections() );
        $this->settings_api->set_fields( $this->get_settings_fields() );

        //initialize settings
        $this->settings_api->admin_init();
    }

    function admin_menu() {
        add_options_page( 'PureDevs GDPR Compliance', 'PureDevs GDPR Compliance', 'delete_posts', 'pure_gdpr_cookie_settings', array($this, 'show_settings_panel') );
    }

	function pd_admin_bar_menu() {
		global $wp_admin_bar;

		$menu_id = 'pure_gdpr_cookie_settings';
		$wp_admin_bar->add_menu( array(
			'id'    => $menu_id,
			'title' => __( 'Pure GDPR Cookie Settings' ),
			'href'  => admin_url() . 'options-general.php?page=pure_gdpr_cookie_settings'
		) );
	}

    function get_settings_sections() {
        $sections = array(
            array(
                'id'    => 'puredevs_general_settings',
                'title' => __( 'General Setting', 'pd_gdpr' ),
            ),
            array(
                'id'    => 'puredevs_banner_settings',
                'title' => __( 'Banner Settings', 'pd_gdpr' ),
            ),
            array(
                'id'    => 'puredevs_button_settings',
                'title' => __( 'Button Settings', 'pd_gdpr' ),
            ),
            array(
                'id'    => 'puredevs_privacy_overview_settings',
                'title' => __( 'Privacy Overview Settings', 'pd_gdpr' ),
            ),
            array(
                'id'    => 'puredevs_strictly_necessary_cookie_settings',
                'title' => __( 'Strictly Necessary Cookie', 'pd_gdpr' ),
            ),
            array(
                'id'    => 'puredevs_additional_cookie_settings',
                'title' => __( 'Additional Cookie Settings', 'pd_gdpr' ),
            ),
            array(
                'id'    => 'puredevs_3rd_party_cookie_settings',
                'title' => __( '3rd Party Cookie Settings', 'pd_gdpr' ),
            ),
            array(
                'id'    => 'puredevs_functional_cookie_settings',
                'title' => __( 'Functional Cookies Settings', 'pd_gdpr' ),
            ),
            array(
                'id'    => 'puredevs_required_cookies_settings',
                'title' => __( 'Required Cookies Settings', 'pd_gdpr' ),
            ),
            array(
                'id'    => 'puredevs_export_import_settings',
                'title' => __( 'Export Import Settings', 'pd_gdpr' ),
            ),
            array(
                'id'    => 'puredevs_geo_location_settings',
                'title' => __( 'Geo Location Settings', 'pd_gdpr' ),
            ),
            /*array(
                'id'    => 'puredevs_analytics_settings',
                'title' => __( 'Analytics Settings', 'pd_gdpr' ),
            ),*/
            array(
                'id'    => 'puredevs_privacy_and_policy_settings',
                'title' => __( 'Privacy and Policy Settings', 'pd_gdpr' ),
            ),
            array(
                'id'    => 'puredevs_help_settings',
                'title' => __( 'Help, Hooks, Shortcodes', 'pd_gdpr' ),
            ),
        );
        return $sections;
    }

    /**
     * Returns all the settings fields
     *
     * @return array settings fields
     */
    function get_settings_fields() {
        $settings_fields = array(
            'puredevs_general_settings' => array(
                array(
                    'name'  => 'primary_status',
                    'label' => __( 'Primary Status', 'pd_gdpr' ),
                    'desc'  => __( '<br/>Allow you to turn on or off the plugin', 'pd_gdpr' ),
                    'type'  => 'checkbox',
                    'default'           => 'on',
                ),
                array(
                    'name'  => 'show_cookie_bar',
                    'label' => __( 'Show Cookie Bar', 'pd_gdpr' ),
                    'desc'  => __( 'Show cookie bar', 'pd_gdpr' ),
                    'type'  => 'checkbox',
                    'default'           => 'on',
                ),
                array(
                    'name'    => 'cookie_bar_style',
                    'label'   => __( 'Cookie Bar Style', 'pd_gdpr' ),
                    'desc'    => __( 'Allow you to show the cookie bar either as a banner or a popup', 'pd_gdpr' ),
                    'type'    => 'select',
                    'default' => 'banner',
                    'options' => array(
                        'banner' => 'Banner',
                        'popup'  => 'Popup',
                    ),
                    'class' => 'form-control bar-style border-primary mb-2',
                ),
                array(
                    'name'    => 'cookie_bar_position',
                    'label'   => __( 'Cookie Bar Position', 'pd_gdpr' ),
                    'desc'    => __( 'Allow you to choose the position where the cookie ber will be shown.', 'pd_gdpr' ),
                    'type'    => 'select',
                    'default' => 'footer',
                    'options' => array(
                        'header' => 'Header',
                        'footer'  => 'Footer',
                    ),
                    'class' => 'form-control  border-primary mb-2 bar-position-wrap',
                ),
                array(
                    'name'    => 'barshowafter',
                    'label'   => __( 'Show Cookie Alert as', 'pd_gdpr' ),
                    'desc'    => __( 'Allow you to choose the style of cookie alert.', 'pd_gdpr' ),
                    'type'    => 'radio',
                    'default' => 'soe',
                    'options' => array(
                        'soe' => 'Scroll bound',
                        'time'  => 'Time bound',
                    ),
                    'class' => 'custom-control-input custom-radio show-cookie-bar',
                ),
                array(
                    'name'              => 'scroll_offset',
                    'label'             => __( 'Scroll Offset ', 'pd_gdpr' ),
                    'desc'              => __( 'Numbers are calculated in pixels', 'pd_gdpr' ),
                    'placeholder'       => __( '120', 'pd_gdpr' ),
                    'type'              => 'text',
                    'default'           => '120',
                    'sanitize_callback' => 'sanitize_text_field',
                    'class' => 'form-control border-primary rounded mb-2 bar-position-wrapper show-cookie-bar-option',
                ),
                array(
                    'name'              => 'cookie_bar_time',
                    'label'             => __( 'Cookie Bar / Time ', 'pd_gdpr' ),
                    'desc'              => __( 'Numbers are calculated in microseconds (ms)', 'pd_gdpr' ),
                    'placeholder'       => __( '2000', 'pd_gdpr' ),
                    'type'              => 'text',
                    'default'           => '2000',
                    'sanitize_callback' => 'sanitize_text_field',
                    'class' => 'form-control border-primary rounded mb-2 bar-time-wrapper show-cookie-bar-option',
                ),
                array(
                    'name'  => 'floating_button_heading',
                    'value' => 'Floating Button Settings',
                    'type'  => 'heading',
                    'class' => 'heading',
                ),
                array(
                    'name'  => 'floating_button_status',
                    'label' => __( 'Floating Button Status', 'pd_gdpr' ),
                    'desc'  => __( '', 'pd_gdpr' ),
                    'type'  => 'checkbox',
                    'default' => '',
                ),
                array(
                    'name'    => 'floating_button_position',
                    'label'   => __( 'Floating Button Position', 'pd_gdpr' ),
                    'desc'    => __( 'Allow you set the position of the floating button', 'pd_gdpr' ),
                    'type'    => 'select',
                    'default' => 'bottom-left',
                    'options' => array(
                        'top-right' => 'Top Right',
                        'top-left'  => 'Top Left',
                        'bottom-right'  => 'Bottom Right',
                        'bottom-left'  => 'Bottom Left',
                    ),
                    'class' => 'form-control border-primary mb-2',
                ),
                array(
                    'name'    => 'floating_button_color',
                    'label'   => __( 'Floating Button Color', 'pd_gdpr' ),
                    'desc'    => __( 'Set the color of the floating button', 'pd_gdpr' ),
                    'type'    => 'color',
                    'default' => '#445454'
                ),
                array(
                    'name'    => 'floating_button_show_after',
                    'label'   => __( 'Floating Button\'s Delay Options', 'pd_gdpr' ),
                    'desc'    => __( 'Allow you to choose when to show the floating button', 'pd_gdpr' ),
                    'type'    => 'radio',
                    'default' => 'soe',
                    'options' => array(
                        'soe' => 'Scroll bound',
                        'time'  => 'Time bound',
                    ),
                    'class' => 'custom-control-input custom-radio show-floating-button',
                ),
                array(
                    'name'              => 'floating_button_scroll_offset',
                    'label'             => __( 'Set Floating Button\'s Scroll Effect', 'pd_gdpr' ),
                    'desc'              => __( 'Numbers are calculated in pixels', 'pd_gdpr' ),
                    'placeholder'       => __( '120', 'pd_gdpr' ),
                    'type'              => 'text',
                    'default'           => '120',
                    'sanitize_callback' => 'sanitize_text_field',
                    'class' => 'form-control border-primary rounded mb-2 floating-bar-position-wrapper show-floating-bar-option',
                ),
                array(
                    'name'              => 'floating_button_show_in_delay',
                    'label'             => __( 'Set floating button\'s delay time', 'pd_gdpr' ),
                    'desc'              => __( 'Time is calculated in microseconds(ms) ', 'pd_gdpr' ),
                    'placeholder'       => __( '5000', 'pd_gdpr' ),
                    'type'              => 'text',
                    'default'           => '',
                    'sanitize_callback' => 'sanitize_text_field',
                    'class' => 'form-control border-primary rounded mb-2 floating-bar-time-wrapper show-floating-bar-option',
                ),
                array(
                    'name'    => 'selected_cookie_categories',
                    'label'   => __( 'Cookie Categories', 'pd_gdpr' ),
                    'desc'    => __( 'Cookie Categories', 'pd_gdpr' ),
                    'type'    => 'multicheck',
                    'default' => array(
                        'strictly_necessary_cookie' => 'strictly_necessary_cookie',
                        'additional_cookie' => 'additional_cookie',
                        '3rd_party_cookie' => '3rd_party_cookie',
                        'required_cookies' => 'required_cookies',
                    ),
                    'options' => array(
                        'strictly_necessary_cookie'   => 'Strictly Necessary Cookie',
                        'additional_cookie'   => 'Additional Cookie',
                        '3rd_party_cookie' => '3rd party Cookie',
                        'functional_cookie'  => 'Functional Cookies',
                        'required_cookies'  => 'Required Cookies',
                    )
                ),
            ),
            'puredevs_banner_settings' => array(
                array(
                    'name'    => 'banner_content',
                    'label'   => __( 'Banner Content', 'pd_gdpr' ),
                    'desc'    => __( 'Use the following shortcodes to link with setting section: [settings color="#fff"]', 'pd_gdpr' ),
                    'type'    => 'wysiwyg',
                    'default' => 'This website uses cookies to provide you with the best browsing experience. Find out more or adjust your [settings].'
                ),
                array(
                    'name'    => 'banner_background_color',
                    'label'   => __( 'Background Color', 'pd_gdpr' ),
                    'desc'    => __( 'Choose the background color of the banner', 'pd_gdpr' ),
                    'type'    => 'color',
                    'default' => '#172b4d'
                ),
                array(
                    'name'    => 'banner_text_color',
                    'label'   => __( 'Text Color', 'pd_gdpr' ),
                    'desc'    => __( 'Choose the text color of the banner', 'pd_gdpr' ),
                    'type'    => 'color',
                    'default' => '#ffffff'
                ),
            ),
            'puredevs_button_settings' => array(
                array(
                    'name'  => 'accept_button_settings',
                    'value' => 'Accept Button Settings',
                    'type'  => 'heading',
                    'class' => 'heading',
                ),
                array(
                    'name'    => 'accept_button_text',
                    'label'   => __( 'Button Text', 'pd_gdpr' ),
                    'desc'    => __( 'Accept Button Text', 'pd_gdpr' ),
                    'placeholder'       => __( 'Accept', 'pd_gdpr' ),
                    'type'              => 'text',
                    'default'           => 'Accept',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'    => 'accept_button_text_color',
                    'label'   => __( 'Text Color', 'pd_gdpr' ),
                    'desc'    => __( 'Choose the text color of the accept button', 'pd_gdpr' ),
                    'type'    => 'color',
                    'default' => ''
                ),
                array(
                    'name'    => 'accept_button_background_color',
                    'label'   => __( 'Background Color', 'pd_gdpr' ),
                    'desc'    => __( 'Choose the background color of the accept button', 'pd_gdpr' ),
                    'type'    => 'color',
                    'default' => ''
                ),
                array(
                    'name'  => 'reject_button_settings',
                    'value' => 'Reject Button Settings',
                    'type'  => 'heading',
                    'class' => 'heading',
                ),
                array(
                    'name'    => 'reject_button_text',
                    'label'   => __( 'Button Text', 'pd_gdpr' ),
                    'desc'    => __( 'Reject Button Text', 'pd_gdpr' ),
                    'placeholder'       => __( 'Reject', 'pd_gdpr' ),
                    'type'              => 'text',
                    'default'           => '',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'    => 'reject_button_text_color',
                    'label'   => __( 'Text Color', 'pd_gdpr' ),
                    'desc'    => __( 'Choose the text color of the reject button', 'pd_gdpr' ),
                    'type'    => 'color',
                    'default' => ''
                ),
                array(
                    'name'    => 'reject_button_background_color',
                    'label'   => __( 'Background Color', 'pd_gdpr' ),
                    'desc'    => __( 'Choose the background color of the reject button', 'pd_gdpr' ),
                    'type'    => 'color',
                    'default' => ''
                ),
                array(
                    'name'  => 'show_reject_button',
                    'label' => __( 'Show Reject Button?', 'pd_gdpr' ),
                    'desc'  => __( 'Choose whether to show the reject button or not', 'pd_gdpr' ),
                    'type'  => 'checkbox',
                    'default'           => '',
                ),
            ),
            'puredevs_privacy_overview_settings' => array(
                /*array(
                    'name'  => 'enable_disable_po_settings',
                    'label' => __( 'ENABLE / DISABLE', 'pd_gdpr' ),
                    'desc'  => __( '', 'pd_gdpr' ),
                    'type'  => 'checkbox',
                    'default'           => '',
                ),*/
                array(
                    'name'    => 'privacy_overview_tab_title',
                    'label'   => __( 'Tab Title', 'pd_gdpr' ),
                    'desc'    => __( 'Tab Title', 'pd_gdpr' ),
                    'type'              => 'text',
                    'default'           => 'Privacy Overview',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'    => 'privacy_overview_tab_content',
                    'label'   => __( 'Tab Content', 'pd_gdpr' ),
                    'desc'    => __( 'Tab Content', 'pd_gdpr' ),
                    'type'    => 'wysiwyg',
                    'default' => __( 'This website uses cookies so that we can provide you with the best user experience possible. Cookie information is stored in your browser and performs functions such as recognising you when you return to our website and helping our team to understand which sections of the website you find most interesting and useful.', 'pd_gdpr' ),
                ),
            ),
            'puredevs_strictly_necessary_cookie_settings' => array(
                array(
                    'name'  => 'snc_choose_functionality',
                    'label' => __( 'Choose Functionality', 'pd_gdpr' ),
                    'desc'  => __( '', 'pd_gdpr' ),
                    'type'    => 'radio',
                    'default' => 'always-enable',
                    'options' => array(
                        'optional' => 'Optional (User select their preferences)',
                        'always-enable' => 'Always enabled (User can not disable but can see the content)',
                        'always-enable-chfu'  => 'Always enabled and content is hidden from the user',
                    ),
                ),
                array(
                    'name'    => 'snec_tab_title',
                    'label'   => __( 'Tab Title', 'pd_gdpr' ),
                    'desc'    => __( 'Tab Title', 'pd_gdpr' ),
                    'default'           => __( 'Strictly Necessary Cookies', 'pd_gdpr' ),
                    'type'              => 'text',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'        => 'snec_tab_content',
                    'label'       => __( 'Tab Content', 'pd_gdpr' ),
                    'desc'        => __( 'Tab Content', 'pd_gdpr' ),
                    'default' => __( 'Strictly Necessary Cookie should be enabled at all times so that we can save your preferences for cookie settings.', 'pd_gdpr' ),
                    'type'        => 'wysiwyg'
                ),
                array(
                    'name'        => 'snec_content',
                    'label'       => __( 'Strictly Necessary Sub Category Cookies', 'pd_gdpr' ),
                    'desc'        => __( 'Tab Content', 'pd_gdpr' ),
                    'default' => __( 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'pd_gdpr' ),
                    'type'        => 'wysiwyg'
                ),
            ),
            'puredevs_additional_cookie_settings' => array(
                array(
                    'name'  => 'enable_disable_additional_cookie',
                    'label' => __( 'ENABLE / DISABLE', 'pd_gdpr' ),
                    'desc'  => __( '', 'pd_gdpr' ),
                    'type'  => 'checkbox',
                    'default'           => '',
                ),
                array(
                    'name'    => 'additional_cookie_tab_title',
                    'label'   => __( 'Tab Title', 'pd_gdpr' ),
                    'desc'    => __( 'Tab Title', 'pd_gdpr' ),
                    'default'       => __( 'Additional Cookie', 'pd_gdpr' ),
                    'type'              => 'text',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'        => 'additional_cookie_tab_content',
                    'label'       => __( 'Tab Content', 'pd_gdpr' ),
                    'desc'        => __( 'Tab Content', 'pd_gdpr' ),
                    'default' => __( 'Additional Cookies should be enabled at all times so that we can save your preferences for cookie settings.', 'pd_gdpr' ),
                    'type'        => 'wysiwyg'
                ),
                array(
                    'name'  => 'additional_cookie_tabs_header_text',
                    'value' => 'Paste your codes and snippets below. They will be added to all pages if user enables these cookies:',
                    'type'  => 'heading',
                    'class' => 'heading',
                ),
                array(
                    'name'        => 'additional_cookie_head_section',
                    'label'       => __( 'Head Section', 'pd_gdpr' ),
                    'desc'        => __( 'For example, you can use it for Google Tag Manager script or any other 3rd party code snippets.', 'pd_gdpr' ),
                    'placeholder' => __( 'Strictly necessary cookies should be enabled at all times.', 'pd_gdpr' ),
                    'type'        => 'textarea'
                ),
                array(
                    'name'        => 'additional_cookie_body_section',
                    'label'       => __( 'Body Section', 'pd_gdpr' ),
                    'desc'        => __( 'For example, you can use it for Google Tag Manager script or any other 3rd party code snippets.', 'pd_gdpr' ),
                    'placeholder' => __( 'Strictly necessary cookies should be enabled at all times.', 'pd_gdpr' ),
                    'type'        => 'textarea'
                ),
                array(
                    'name'        => 'additional_cookie_footer_section',
                    'label'       => __( 'Footer Section', 'pd_gdpr' ),
                    'desc'        => __( 'For example, you can use it for Google Tag Manager script or any other 3rd party code snippets.', 'pd_gdpr' ),
                    'placeholder' => __( 'Strictly necessary cookies should be enabled at all times.', 'pd_gdpr' ),
                    'type'        => 'textarea'
                ),
                array(
                    'name'        => 'additional_cookie_content',
                    'label'       => __( 'Additional Sub Category Cookies', 'pd_gdpr' ),
                    'desc'        => __( 'Tab Content', 'pd_gdpr' ),
                    'default' => __( 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'pd_gdpr' ),
                    'type'        => 'wysiwyg'
                ),
            ),
            'puredevs_3rd_party_cookie_settings' => array(
                array(
                    'name'  => 'enable_disable_3rd_party_cookie',
                    'label' => __( 'ENABLE / DISABLE', 'pd_gdpr' ),
                    'desc'  => __( '', 'pd_gdpr' ),
                    'type'  => 'checkbox',
                    'default'           => '',
                ),
                array(
                    'name'    => '3rd_party_cookie_tab_title',
                    'label'   => __( 'Tab Title', 'pd_gdpr' ),
                    'desc'    => __( 'Tab Title', 'pd_gdpr' ),
                    'default'       => __( '3rd party Cookie', 'pd_gdpr' ),
                    'type'              => 'text',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'        => '3rd_party_cookie_tab_content',
                    'label'       => __( 'Tab Content', 'pd_gdpr' ),
                    'desc'        => __( 'Tab Content', 'pd_gdpr' ),
                    'default' => __( '3rd Party Cookies should be enabled at all times so that we can save your preferences for cookie settings.', 'pd_gdpr' ),
                    'type'        => 'wysiwyg'
                ),
                array(
                    'name'  => '3rd_party_cookie_tabs_header_text',
                    'value' => 'Paste your codes and snippets below. They will be added to all pages if user enables these cookies:',
                    'type'  => 'heading',
                    'class' => 'heading',
                ),
                array(
                    'name'        => '3rd_party_cookie_head_section',
                    'label'       => __( 'Head Section', 'pd_gdpr' ),
                    'desc'        => __( 'For example, you can use it for Google Tag Manager script or any other 3rd party code snippets.', 'pd_gdpr' ),
                    'placeholder' => __( '3rd party cookies should be enabled at all times.', 'pd_gdpr' ),
                    'type'        => 'textarea'
                ),
                array(
                    'name'        => '3rd_party_cookie_body_section',
                    'label'       => __( 'Body Section', 'pd_gdpr' ),
                    'desc'        => __( 'For example, you can use it for Google Tag Manager script or any other 3rd party code snippets.', 'pd_gdpr' ),
                    'placeholder' => __( '3rd party cookies should be enabled at all times.', 'pd_gdpr' ),
                    'type'        => 'textarea'
                ),
                array(
                    'name'        => '3rd_party_cookie_footer_section',
                    'label'       => __( 'Footer Section', 'pd_gdpr' ),
                    'desc'        => __( 'For example, you can use it for Google Tag Manager script or any other 3rd party code snippets.', 'pd_gdpr' ),
                    'placeholder' => __( '3rd party cookies should be enabled at all times.', 'pd_gdpr' ),
                    'type'        => 'textarea'
                ),
                array(
                    'name'        => '3rd_party_cookie_content',
                    'label'       => __( '3rd Party Sub Category Cookies', 'pd_gdpr' ),
                    'desc'        => __( 'Tab Content', 'pd_gdpr' ),
                    'default' => __( 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'pd_gdpr' ),
                    'type'        => 'wysiwyg'
                ),
            ),
            'puredevs_functional_cookie_settings' => array(
                array(
                    'name'  => 'enable_disable_functional_cookie',
                    'label' => __( 'ENABLE / DISABLE', 'pd_gdpr' ),
                    'desc'  => __( '', 'pd_gdpr' ),
                    'type'  => 'checkbox',
                    'default'           => '',
                ),
                array(
                    'name'    => 'functional_cookie_tab_title',
                    'label'   => __( 'Tab Title', 'pd_gdpr' ),
                    'desc'    => __( 'Tab Title', 'pd_gdpr' ),
                    'default'       => __( 'Functional cookie', 'pd_gdpr' ),
                    'type'              => 'text',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'        => 'functional_cookie_tab_content',
                    'label'       => __( 'Tab Content', 'pd_gdpr' ),
                    'desc'        => __( 'Tab Content', 'pd_gdpr' ),
                    'default' => __( 'Functional Cookies should be enabled at all times so that we can save your preferences for cookie settings.', 'pd_gdpr' ),
                    'type'        => 'wysiwyg'
                ),
                array(
                    'name'  => 'functional_cookie_tabs_header_text',
                    'value' => 'Paste your codes and snippets below. They will be added to all pages if user enables these cookies:',
                    'type'  => 'heading',
                    'class' => 'heading',
                ),
                array(
                    'name'        => 'functional_cookie_head_section',
                    'label'       => __( 'Head Section', 'pd_gdpr' ),
                    'desc'        => __( 'For example, you can use it for Google Tag Manager script or any other 3rd party code snippets.', 'pd_gdpr' ),
                    'placeholder' => __( '3rd party cookies should be enabled at all times.', 'pd_gdpr' ),
                    'type'        => 'textarea'
                ),
                array(
                    'name'        => 'functional_cookie_body_section',
                    'label'       => __( 'Body Section', 'pd_gdpr' ),
                    'desc'        => __( 'For example, you can use it for Google Tag Manager script or any other 3rd party code snippets.', 'pd_gdpr' ),
                    'placeholder' => __( '3rd party cookies should be enabled at all times.', 'pd_gdpr' ),
                    'type'        => 'textarea'
                ),
                array(
                    'name'        => 'functional_cookie_footer_section',
                    'label'       => __( 'Footer Section', 'pd_gdpr' ),
                    'desc'        => __( 'For example, you can use it for Google Tag Manager script or any other 3rd party code snippets.', 'pd_gdpr' ),
                    'placeholder' => __( '3rd party cookies should be enabled at all times.', 'pd_gdpr' ),
                    'type'        => 'textarea'
                ),
                array(
                    'name'        => 'functional_cookie_content',
                    'label'       => __( 'Functional Cookie Sub Category Cookies', 'pd_gdpr' ),
                    'desc'        => __( 'Tab Content', 'pd_gdpr' ),
                    'default' => __( 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'pd_gdpr' ),
                    'type'        => 'wysiwyg'
                ),
            ),
            'puredevs_required_cookies_settings' => array(
                array(
                    'name'  => 'enable_disable_required_cookies',
                    'label' => __( 'ENABLE / DISABLE', 'pd_gdpr' ),
                    'desc'  => __( '', 'pd_gdpr' ),
                    'type'  => 'checkbox',
                    'default'           => '',
                ),
                array(
                    'name'    => 'required_cookies_tab_title',
                    'label'   => __( 'Tab Title', 'pd_gdpr' ),
                    'desc'    => __( 'Tab Title', 'pd_gdpr' ),
                    'default'       => __( 'Required cookies', 'pd_gdpr' ),
                    'type'              => 'text',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'        => 'required_cookies_tab_content',
                    'label'       => __( 'Tab Content', 'pd_gdpr' ),
                    'desc'        => __( 'Tab Content', 'pd_gdpr' ),
                    'default' => __( 'Required cookies should be enabled at all times so that we can save your preferences for cookie settings.', 'pd_gdpr' ),
                    'type'        => 'wysiwyg'
                ),
                array(
                    'name'  => 'required_cookies_tabs_header_text',
                    'value' => 'Paste your codes and snippets below. They will be added to all pages if user enables these cookies:',
                    'type'  => 'heading',
                    'class' => 'heading',
                ),
                array(
                    'name'        => 'required_cookies_head_section',
                    'label'       => __( 'Head Section', 'pd_gdpr' ),
                    'desc'        => __( 'For example, you can use it for Google Tag Manager script or any other 3rd party code snippets.', 'pd_gdpr' ),
                    'placeholder' => __( '3rd party cookies should be enabled at all times.', 'pd_gdpr' ),
                    'type'        => 'textarea'
                ),
                array(
                    'name'        => 'required_cookies_body_section',
                    'label'       => __( 'Body Section', 'pd_gdpr' ),
                    'desc'        => __( 'For example, you can use it for Google Tag Manager script or any other 3rd party code snippets.', 'pd_gdpr' ),
                    'placeholder' => __( '3rd party cookies should be enabled at all times.', 'pd_gdpr' ),
                    'type'        => 'textarea'
                ),
                array(
                    'name'        => 'required_cookies_footer_section',
                    'label'       => __( 'Footer Section', 'pd_gdpr' ),
                    'desc'        => __( 'For example, you can use it for Google Tag Manager script or any other 3rd party code snippets.', 'pd_gdpr' ),
                    'placeholder' => __( '3rd party cookies should be enabled at all times.', 'pd_gdpr' ),
                    'type'        => 'textarea'
                ),
                array(
                    'name'        => 'required_cookies_content',
                    'label'       => __( 'Required Cookie Sub Category Cookies', 'pd_gdpr' ),
                    'desc'        => __( 'Tab Content', 'pd_gdpr' ),
                    'default' => __( 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'pd_gdpr' ),
                    'type'        => 'wysiwyg'
                ),
            ),
            'puredevs_export_import_settings' => array(
                array(
                    'name'  => 'export_settings',
                    'label' => __( 'Export Settings', 'pd_gdpr' ),
                    'desc'  => __( '', 'pd_gdpr' ),
                    'type'    => 'radio',
                    'default' => 'all',
                    'options' => array(
                        'all' => 'All',
                        'custom' => 'Custom',
                    ),
                    'class' => 'form-control border-primary w-98 rounded export-import-section',
                ),
                array(
                    'name'    => 'export_setting_options',
                    'type'    => 'select',
                    'default' => 'puredevs_general_settings',
                    'options' => array(
                        'puredevs_general_settings' => 'General Settings',
                        'puredevs_banner_settings' => 'Banner Settings',
                        'puredevs_button_settings' => 'Button Settings',
                        'puredevs_privacy_overview_settings' => 'Privacy Overview Settings',
                        'puredevs_strictly_necessary_cookie_settings'  => 'Strictly Necessary Cookie',
                        'puredevs_additional_cookie_settings'  => 'Additional Cookie',
                        'puredevs_3rd_party_cookie_settings'  => '3rd party Cookie',
                        'puredevs_functional_cookie_settings'  => 'Functional Cookies',
                        'puredevs_required_cookies_settings'  => 'Required Cookies',
                        'puredevs_geo_location_settings'  => 'Geo Location Settings',
                        'puredevs_privacy_and_policy_settings'  => 'Privacy and Policy Settings',
                    ),
                    'class' => 'ei-opt-wrap d-none',
                ),
                array(
                    'name'    => 'export_button',
                    'value'   => 'EXPORT',
                    'type'    => 'button',
                    'class' => 'btn btn-primary float-right mr-3 export-btn',
                ),
                array(
                    'name'    => 'file',
                    'label'   => __( 'Import Settings', 'pd_gdpr' ),
                    'desc'    => __( 'Import Settings', 'pd_gdpr' ),
                    'type'    => 'file',
                    'default' => '',
                    'options' => array(
                        'button_label' => 'IMPORT',
                    ),
                ),
            ),
            'puredevs_geo_location_settings' => array(
                array(
                    'name'  => 'geo_location_settings',
                    'label' => __( 'Show Cookie Banner', 'pd_gdpr' ),
                    'desc'  => '',
                    'type'    => 'radio',
                    'default' => 'all-users',
                    'options' => array(
                        'all-users' => 'Show to all users [default]',
                        'european-union-only' => "Show to European Union only<br/>(users from outside the EU won't see the Cookie Notice and will have all cookies enabled by default)",
                    ),
                    'class' => 'form-control border-primary w-98 rounded geo_location-section',
                ),
	            array(
		            'name'    => 'geo_location_api',
		            'label'   => __( 'Your IP Geo Location API Key', 'pd_gdpr' ),
		            'desc'    => __( 'To get the most accurate performance from the geolocation feature, go to <a href="https://ipgeolocation.io/" target="_blank">ipgelocation.io</a>. Upon signup, they will provide a key. Put that in Your IP Geo Location API Key and you are good to go.', 'pd_gdpr' ),
		            'type'              => 'text',
		            'default'           => '',
		            'sanitize_callback' => 'sanitize_text_field'
	            ),
            ),
            'puredevs_privacy_and_policy_settings' => array(
                array(
                    'name'  => 'enable_disable_privacy_and_policy',
                    'label' => __( 'ENABLE / DISABLE', 'pd_gdpr' ),
                    'desc'  => __( '', 'pd_gdpr' ),
                    'type'  => 'checkbox',
                    'default'           => 'on',
                ),
                array(
                    'name'    => 'privacy_and_policy_tab_title',
                    'label'   => __( 'Tab Title', 'pd_gdpr' ),
                    'desc'    => __( 'Tab Title', 'pd_gdpr' ),
                    'default'       => __( 'Cookie Policy', 'pd_gdpr' ),
                    'type'              => 'text',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'        => 'privacy_and_policy_message',
                    'label'       => __( 'Strictly Necessary Required Messages', 'pd_gdpr' ),
                    'desc'        => __( 'This warning message will be displayed if the strictly necessary cookies are not enabled and the user try to enable the "Third Party Cookies"', 'pd_gdpr' ),
                    'default' => __( 'Raw denim you probably haven\'t heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Reprehenderit butcher retro keffiyeh dreamcatcher synth.', 'pd_gdpr' ),
                    'type'        => 'wysiwyg'
                ),
                array(
                    'name'    => 'cookie_policy_page_url',
                    'label'   => __( 'Cookie Policy Page Url', 'pd_gdpr' ),
                    'desc'    => __( '', 'pd_gdpr' ),
                    'placeholder'       => __( 'cookie policy page url', 'pd_gdpr' ),
                    'type'              => 'text',
                    'default'           => '',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
            ),
            'puredevs_help_settings' => array(
                array(
                    'name'        => 'html',
                    'desc'        => __( '<ul class="tab clearfix">
                        <li class="tab-item "><a class="active" href="javascript:void(0);" data-tag="faq">FAQ</a></li>
                        <li class="tab-item"><a href="javascript:void(0);" data-tag="hooks">Hooks</a></li>
                        <li class="tab-item"><a href="javascript:void(0);" data-tag="shortcodes">Shortcodes</a></li>
                    </ul>

                    <div class="tab-wrapper ">
                           <div class="list" id="faq">
                               <ul class="custom-accordian">
                                   <li>
                                       <a class="acc_trigger  ">
                                           How do I setup your plugin?
                                           <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body ">
                                               <p class="font-weight-500"> To setup your plugin, first activate puredevs gdpr complience plugin. Then go to <span class="text-danger font-weight-500">Settings -> PureDevs GDPR Compliance </span>. In the general settings tab, you need to enable primary status.</p>
                                           </div>
                                       </div>
                                   </li>
                                   
                                   <li>
                                       <a class="  acc_trigger ">Can I use custom code or hooks with your plugin?               
                                           <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body">
                                               <p class="font-weight-500">Yes. We have implemented hooks that allow you to implement custom scripts, for some examples see the list of pre-defined hooks here: <strong> Hooks tab</strong></p>
                                           </div>
                                       </div>
                                   </li>
                                   
                                   <li>
                                       <a class="  acc_trigger ">Does the plugin support subdomains?            
                                           <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body">
                                               <p class="font-weight-500">Unfortunately not, subdomains are treated as separate domains by browsers and we’re unable to change the cookies stored by another domain. If your multisite setup use subdomain version, each subsite will be recognised as a separate domain by the browser and will create cookies for each subdomain.</p>
                                           </div>
                                       </div>
                                   </li>
                               </ul>
                           </div>
                           <div class="list hide" id="hooks">
                               <ul class="custom-accordian">
                                   <li>
                                       <a class="acc_trigger  ">HOOK to GDPR custom 3RD-PARTY script by php – HEAD
                                           <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body ">
                                               <div class="code-panel m-2">
                                                    <code>
                                                       add_filter(\'pd_gdpr_3rd_party_header_assets\',\'pd_gdpr_3rd_party_header_assets_function\');
                                                        <br>
                                                        function pd_gdpr_3rd_party_header_assets_function( $scripts ) {
                                                            $scripts .= \'<script>console.log("third-party-head");</script>\';
                                                            return $scripts;
                                                         }
                                                     </code>
                                                </div>
                                           </div>
                                       </div>
                                   </li>
                                   <li>
                                       <a class="acc_trigger  ">HOOK to GDPR custom 3RD-PARTY script by php – BODY
                                           <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body ">
                                               <div class="code-panel m-2">
                                                    <code>
                                                       add_filter(\'pd_gdpr_3rd_party_body_assets\',\'pd_gdpr_3rd_party_body_assets_function\');
                                                        <br>
                                                        function pd_gdpr_3rd_party_body_assets_function( $scripts ) {
                                                            $scripts .= \'<script>console.log("third-party-body");</script>\';
                                                            return $scripts;
                                                         }
                                                     </code>
                                                </div>
                                           </div>
                                       </div>
                                   </li>
                                   <li>
                                       <a class="acc_trigger  ">HOOK to GDPR custom 3RD-PARTY script by php – FOOTER
                                           <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body ">
                                               <div class="code-panel m-2">
                                                    <code>
                                                       add_filter(\'pd_gdpr_3rd_party_footer_assets\',\'pd_gdpr_3rd_party_footer_assets_function\');
                                                        <br>
                                                        function pd_gdpr_3rd_party_footer_assets_function( $scripts ) {
                                                            $scripts .= \'<script>console.log("third-party-footer");</script>\';
                                                            return $scripts;
                                                         }
                                                     </code>
                                                </div>
                                           </div>
                                       </div>
                                   </li>
                                   <li>
                                       <a class="acc_trigger  ">HOOK to GDPR custom ADDITIONAL COOKIE script by php – HEAD
                                           <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body ">
                                               <div class="code-panel m-2">
                                                    <code>
                                                       add_filter(\'pd_gdpr_additional_cookie_header_assets\',\'pd_gdpr_additional_cookie_header_assets_function\');
                                                        <br>
                                                        function pd_gdpr_additional_cookie_header_assets_function( $scripts ) {
                                                            $scripts .= \'<script>console.log("additional-cookie-head");</script>\';
                                                            return $scripts;
                                                         }
                                                     </code>
                                                </div>
                                           </div>
                                       </div>
                                   </li>
                                   <li>
                                       <a class="acc_trigger  ">HOOK to GDPR custom ADDITIONAL COOKIE script by php – BODY
                                           <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body ">
                                               <div class="code-panel m-2">
                                                    <code>
                                                       add_filter(\'pd_gdpr_additional_cookie_body_assets\',\'pd_gdpr_additional_cookie_body_assets_function\');
                                                        <br>
                                                        function pd_gdpr_additional_cookie_body_assets_function( $scripts ) {
                                                            $scripts .= \'<script>console.log("additional-cookie-body");</script>\';
                                                            return $scripts;
                                                         }
                                                     </code>
                                                </div>
                                           </div>
                                       </div>
                                   </li>
                                   <li>
                                       <a class="acc_trigger  ">HOOK to GDPR custom ADDITIONAL COOKIE script by php – FOOTER
                                           <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body ">
                                               <div class="code-panel m-2">
                                                    <code>
                                                       add_filter(\'pd_gdpr_additional_cookie_footer_assets\',\'pd_gdpr_additional_cookie_footer_assets_function\');
                                                        <br>
                                                        function pd_gdpr_additional_cookie_footer_assets_function( $scripts ) {
                                                            $scripts .= \'<script>console.log("additional-cookie-footer");</script>\';
                                                            return $scripts;
                                                         }
                                                     </code>
                                                </div>
                                           </div>
                                       </div>
                                   </li>
                                   <li>
                                       <a class="acc_trigger  ">HOOK to GDPR custom REQUIRED COOKIE  script by php – HEAD
                                           <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body ">
                                               <div class="code-panel m-2">
                                                    <code>
                                                       add_filter(\'pd_gdpr_required_cookie_header_assets\',\'pd_gdpr_required_cookie_header_assets_function\');
                                                        <br>
                                                        function pd_gdpr_required_cookie_header_assets_function( $scripts ) {
                                                            $scripts .= \'<script>console.log("required-cookie-head");</script>\';
                                                            return $scripts;
                                                         }
                                                     </code>
                                                </div>
                                           </div>
                                       </div>
                                   </li>
                                   <li>
                                       <a class="acc_trigger  ">HOOK to GDPR custom REQUIRED COOKIE script by php – BODY
                                           <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body ">
                                               <div class="code-panel m-2">
                                                    <code>
                                                       add_filter(\'pd_gdpr_required_cookie_body_assets\',\'pd_gdpr_required_cookie_body_assets_function\');
                                                        <br>
                                                        function pd_gdpr_required_cookie_body_assets_function( $scripts ) {
                                                            $scripts .= \'<script>console.log("required-cookie-body");</script>\';
                                                            return $scripts;
                                                         }
                                                     </code>
                                                </div>
                                           </div>
                                       </div>
                                   </li>
                                   <li>
                                       <a class="acc_trigger  ">HOOK to GDPR custom REQUIRED COOKIE script by php – FOOTER
                                           <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body ">
                                               <div class="code-panel m-2">
                                                    <code>
                                                       add_filter(\'pd_gdpr_required_cookie_footer_assets\',\'pd_gdpr_required_cookie_footer_assets_function\');
                                                        <br>
                                                        function pd_gdpr_required_cookie_footer_assets_function( $scripts ) {
                                                            $scripts .= \'<script>console.log("required-cookie-footer");</script>\';
                                                            return $scripts;
                                                         }
                                                     </code>
                                                </div>
                                           </div>
                                       </div>
                                   </li>
                                   <li>
                                       <a class="acc_trigger  ">HOOK to GDPR custom FUNCTIONAL COOKIE script by php – HEAD
                                           <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body ">
                                               <div class="code-panel m-2">
                                                    <code>
                                                       add_filter(\'pd_gdpr_functional_cookie_header_assets\',\'pd_gdpr_functional_cookie_header_assets_function\');
                                                        <br>
                                                        function pd_gdpr_functional_cookie_header_assets_function( $scripts ) {
                                                            $scripts .= \'<script>console.log("functional-cookie-head");</script>\';
                                                            return $scripts;
                                                         }
                                                     </code>
                                                </div>
                                           </div>
                                       </div>
                                   </li>
                                   <li>
                                       <a class="acc_trigger  ">HOOK to GDPR custom FUNCTIONAL COOKIE script by php – BODY
                                           <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body ">
                                               <div class="code-panel m-2">
                                                    <code>
                                                       add_filter(\'pd_gdpr_functional_cookie_body_assets\',\'pd_gdpr_functional_cookie_body_assets_function\');
                                                        <br>
                                                        function pd_gdpr_functional_cookie_body_assets_function( $scripts ) {
                                                            $scripts .= \'<script>console.log("functional-cookie-body");</script>\';
                                                            return $scripts;
                                                         }
                                                     </code>
                                                </div>
                                           </div>
                                       </div>
                                   </li>
                                   <li>
                                       <a class="acc_trigger  ">HOOK to GDPR custom FUNCTIONAL COOKIE script by php – FOOTER
                                           <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body ">
                                               <div class="code-panel m-2">
                                                    <code>
                                                       add_filter(\'pd_gdpr_functional_cookie_footer_assets\',\'pd_gdpr_functional_cookie_footer_assets_function\');
                                                        <br>
                                                        function pd_gdpr_functional_cookie_footer_assets_function( $scripts ) {
                                                            $scripts .= \'<script>console.log("functional-cookie-footer");</script>\';
                                                            return $scripts;
                                                         }
                                                     </code>
                                                </div>
                                           </div>
                                       </div>
                                   </li>
                               </ul>
                           </div>
                           <div class="list hide" id="shortcodes">
                                <ul class="custom-accordian">
                                   <li>
                                       <a class="acc_trigger  ">
                                           How can I link setting popup with the banner message?
                                           <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body ">
                                               <p class="font-weight-500"> To link the setting popup with your banner message, add [settings color="#dd3333"] to your banner content under the <strong>Banner Setting</strong>.</p>
                                           </div>
                                       </div>
                                   </li>

                                   <li>
                                       <a class="  acc_trigger ">How can I change the color of the setting link?
                                            <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body">
                                               <p class="font-weight-500">To change the color of the setting link first find the hexadecimal version of your desired color and replace it with the default color, you can find the value of the default color within the quotation mark in the [settings color="#dd3333"].</p>
                                           </div>
                                       </div>
                                   </li>

                                   <li>
                                       <a class="  acc_trigger ">How can I add or remove subcategory cookie polices from my cookie policy page?
                                           <i class="angle-down"></i>
                                       </a>

                                       <div class="acc_container" >
                                           <div class="acc_card card-body">
                                               <p class="font-weight-500">To remove all or any of the default subcategories from the  cookie policy page, go to edit cookie policy page in wp-admin and remove the shortcode of the subcatagory that you want to get rid of.</p>
                                           </div>
                                       </div>
                                   </li>
                               </ul>
                           </div>
                       </div>', 'pd_gdpr' ),
                    'type'        => 'html'
                ),
            ),
        );

        return $settings_fields;
    }

    function show_settings_panel() {
        echo '<div class="wrap">';
	    echo '<h2>PureDevs GDPR Compliance</h2>';
	    echo '<div class="gdpr-wrapper">';

        $this->settings_api->show_navigation();
        $this->settings_api->show_forms();

        echo '</div>';
        echo '</div>';
    }

    /**
     * Get all the pages
     *
     * @return array page names with key value pairs
     */
    function get_pages() {
        $pages = get_pages();
        $pages_options = array();
        if ( $pages ) {
            foreach ($pages as $page) {
                $pages_options[$page->ID] = $page->post_title;
            }
        }

        return $pages_options;
    }

}
endif;

<?php
add_action( 'wp_ajax_export_data', 'export_data' );
add_action( 'wp_ajax_nopriv_export_data', 'export_data' );
function export_data() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'options';
    if ( isset( $_POST['export_type'] ) && $_POST['export_type'] == 'all' ) {
        $plugin_options = $wpdb->get_results( "SELECT option_name,option_value FROM $table_name WHERE option_name LIKE 'puredevs_%_settings'" );

        $delimiter = ",";
        //$filename = "pd_cookie_settings" . date('Y-m-d') . ".csv";
        //create a file pointer
        $f = fopen( 'php://memory', 'w' );
        //set column headers
        $option_name = wp_list_pluck( $plugin_options, 'option_name' );
        $option_value = wp_list_pluck( $plugin_options, 'option_value' );
        fputcsv($f, $option_name, $delimiter);
        fputcsv($f, $option_value, $delimiter);
        //move back to beginning of file
        fseek($f, 0);

        //set headers to download file rather than displayed
        //header('Content-Type: text/csv');
        //header('Content-Disposition: attachment; filename="' . $filename . '";');

        //output all remaining data on a file pointer
        fpassthru($f);
    } elseif ( isset( $_POST['export_type'] ) && $_POST['export_type'] == 'custom' ) {
        $plugin_options = $wpdb->get_results( "SELECT option_name,option_value FROM $table_name WHERE option_name = '$_POST[select_opt]'" );

        $delimiter = ",";
        //$filename = "pd_cookie_settings" . date('Y-m-d') . ".csv";
        //create a file pointer
        $f = fopen('php://memory', 'w');
        //set column headers
        $option_name = wp_list_pluck( $plugin_options, 'option_name' );
        $option_value = wp_list_pluck( $plugin_options, 'option_value' );
        fputcsv($f, $option_name, $delimiter);
        fputcsv($f, $option_value, $delimiter);
        //move back to beginning of file
        fseek($f, 0);

        //set headers to download file rather than displayed
        //header('Content-Type: text/csv');
        //header('Content-Disposition: attachment; filename="' . $filename . '";');

        //output all remaining data on a file pointer
        fpassthru($f);
    }
    exit();
}